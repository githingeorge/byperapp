module.exports = function(api) {
  //   api.cache(true)
  const isTest = api.env('test')
  const presets = [
    ['@babel/preset-env', { targets: { node: 'current' } }],
    [
      '@babel/preset-typescript',
      {
        isTSX: true,
        jsxPragma: 'h',
        allExtensions: true
      }
    ]
  ]
  const plugins = [
    '@babel/proposal-class-properties',
    '@babel/proposal-object-rest-spread'
  ]
  if (isTest) {
    plugins.push('transform-es2015-modules-commonjs')
  }
  return {
    presets,
    plugins
  }
}
