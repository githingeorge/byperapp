import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import sourceMaps from 'rollup-plugin-sourcemaps'
import { camelCase } from 'lodash'
import typescript from 'rollup-plugin-typescript2'
import json from 'rollup-plugin-json'
import { terser } from 'rollup-plugin-terser'
import replace from 'rollup-plugin-replace'
const pkg = require('./package.json')

const libraryName = 'byperapp'
const production = process.env.NODE_ENV == 'production';
export default {
  input: `src/${libraryName}.ts`,
  output: [
    {
      file: pkg.main,
      name: camelCase(libraryName),
      format: 'umd',
      sourcemap: true,
      esModule: false
    },
    {
      file: pkg.module,
      name: camelCase(libraryName),
      format: 'es',
      sourcemap: true
    }
  ],
  // interop: false,
  // Indicate here external modules you don't wanna include in your bundle (i.e.: 'lodash')
  external: [],
  watch: {
    include: 'src/**'
  },
  plugins: [
    // Allow json resolution
    json(),
    // Allow bundling cjs modules (unlike webpack, rollup doesn't understand cjs)
    commonjs(),
    // Allow node_modules resolution, so you can use 'external' to control
    // which external modules to include in the bundle
    // https://github.com/rollup/rollup-plugin-node-resolve#usage
    resolve({
      browser: true
    }),

    // Compile TypeScript files
    typescript({
      useTsconfigDeclarationDir: true,
      typescript: require('typescript'),
      tsconfigOverride: {
        removeComments: production
      }
    }),
    // Resolve source maps to the original source
    sourceMaps(),

    (production &&  terser()),

    replace({
      exclude: 'node_modules/**',
      ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
    }),


  ]
}
