import { h, app } from 'byperapp'
// alert('test')
// let { h, app } = byperapp
console.log('object :')
const state = {
  todos: [
    {
      id: 1,
      done: false,
      description: 'buy milk'
    },
    {
      id: 2,
      done: false,
      description: 'Go to dance class'
    }
  ],
  completed: [],
  newTodo: ''
}
const actions = {
  ToggleTodo: (state: any, todo: any, event: Event) => {
    // @ts-ignore
    if (event.currentTarget && event.currentTarget.checked) {
      todo.done = true
      state.completed.push(todo)
      state.todos = state.todos.filter((i: any) => i.id != todo.id)
    }
    console.log(state)
    return { ...state }
  },
  UpdateNewTodo: (state: any, event: Event) => ({
    ...state,
    // @ts-ignore
    newTodo: event.currentTarget.value
  }),
  AddTodo: (state, event) => {
    let { todos } = state
    state.todos.push({
      id: todos.length + 1,
      done: false,
      description: state.newTodo
    })
    state.newTodo = ''

    return { ...state }
  }
}
const todosList = ({ todos }) => {
  console.log('todos :', todos)
  const items = todos.map(todo => (
    <div>
      <input type="checkbox" checked={todo.done} />,
      <span> {todo.description}</span>
    </div>
  ))
  console.log('items :', items)
  return items
}
const todoItem = (todo, eventHandler) =>
  h(
    'div',
    {},
    h('input', {
      type: 'checkbox',
      checked: todo.done,
      onchange: eventHandler([actions.ToggleTodo, todo])
    }),
    h('span', {}, todo.description)
  )
// const todosList =
const AddTodo = ({ newTodo, eventHandler }) => {
  return [
    h('input', {
      type: 'text',
      value: newTodo,
      oninput: eventHandler(actions.UpdateNewTodo)
    }),
    h('button', { onclick: eventHandler(actions.AddTodo) }, 'Add')
  ]
}
const view = ({
  state,
  eventHandler
}: {
  state: any
  eventHandler: EventHandler
}) => {
  console.log('state :', state)
  let { todos, completed } = state
  return h(
    'main',
    {},
    h(
      'div',
      {},
      h('h2', {}, 'Todos:'),
      h(AddTodo, { newTodo: state.newTodo, eventHandler }),
      todos.map(todo => todoItem(todo, eventHandler)),
      h('h3', {}, `Completed: ${completed.length}`),
      completed.map(todo => todoItem(todo, eventHandler))
    )
  )
}

app({
  init: state,
  view: view,
  container: document.body
})
