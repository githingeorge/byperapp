var NODE_TYPE;
(function (NODE_TYPE) {
    NODE_TYPE[NODE_TYPE["DEFAULT"] = 0] = "DEFAULT";
    NODE_TYPE[NODE_TYPE["RECYCLED"] = 1] = "RECYCLED";
    NODE_TYPE[NODE_TYPE["LAZY"] = 2] = "LAZY";
    NODE_TYPE[NODE_TYPE["TEXT"] = 3] = "TEXT";
})(NODE_TYPE || (NODE_TYPE = {}));
var EMPTY_OBJECT = {};
var EMPTY_ARRAY = [];

var hooks = ['create', 'update', 'remove', 'destroy'];

var createVNode = function (name, props, children, element, key, type) {
    if (element === void 0) { element = null; }
    if (key === void 0) { key = null; }
    if (type === void 0) { type = NODE_TYPE.DEFAULT; }
    // console.log(name + ' => keys:', createKeyMap(children))
    return {
        name: name,
        props: props,
        children: children,
        element: element,
        key: key,
        type: type
    };
};
var createTextVNode = function (text, element) {
    if (element === void 0) { element = null; }
    return createVNode(text, EMPTY_OBJECT, EMPTY_ARRAY, element, null, NODE_TYPE.TEXT);
};
var h = function (name, props) {
    var rest = [];
    for (var _i = 2; _i < arguments.length; _i++) {
        rest[_i - 2] = arguments[_i];
    }
    props = props || EMPTY_OBJECT;
    rest = rest.flat();
    // const children = rest.map
    var children = rest.reduce(function (children, node) {
        if (!(node === false || node === true || node == null)) {
            children.push(typeof node === 'object' ? node : createTextVNode(node));
        }
        return children;
    }, []);
    // while (rest.length > 0) {
    //   let node = rest.pop()
    //   if (node === false || node === true || node == null) {
    //   } else {
    //     children.push(typeof node === 'object' ? node : createTextVNode(node))
    //   }
    // }
    if (typeof name === 'function') {
        var component = name;
        for (var _a = 0, hooks_1 = hooks; _a < hooks_1.length; _a++) {
            var hook = hooks_1[_a];
            if (component.hasOwnProperty(hook)) {
                // @ts-ignore
                props[hook] = component[hook];
            }
        }
        return name(props, children);
    }
    // @ts-ignore
    return createVNode(name, props, children, null, props.key, NODE_TYPE.DEFAULT);
};
// const createKeyMap = (children: any[]) => {
//   return children.length > 0
//     ? children.reduce((keys, node) => {
//         if (keys && node && node.key) keys[node.key] = node
//         else return null
//         return keys
//       }, {})
//     : null
// }

// export { app } from './app'
// export { Time } from './effects/time'

export { h };
//# sourceMappingURL=byperapp.es5.js.map
