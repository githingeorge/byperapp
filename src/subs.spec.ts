import { h, app, Time } from './byperapp'
test(name, (done: any) => {
  console.log(`testing ${name} =>`)
  const state = { index: 0 }

  const actions = {
    update: jest.fn((state: any) => {
      if (state.index === 2) {
        expect(actions.update).toBeCalledTimes(state.index + 1)
        return done()
      }
      return { index: state.index + 1 }
    })
  }
  // const effect = Time.delay({ action: actions.next, interval: 20 })

  const view = (state: any) => {
    return h('h1', {}, state.index)
  }

  app({
    init: state,
    view: view,
    subscriptions: (state: any) => [
      state.index < 3 && Time.tick({ interval: 50, action: actions.update })
    ],
    container: document.body
  })
})
