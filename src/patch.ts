// import { VNode, EventCb } from './global'
// import { resolveNode } from './h'
// import { createElement, removeElement, updateElement } from './element'
// import { isTextNode, getKey } from './utils'
// import { NODE_TYPE } from './constants'

// export const patch = function(
//   container: Element,
//   element: Element,
//   oldNode: VNode | null,
//   newNode: VNode,
//   eventCb: EventCb
// ) {
//   return (element = patchElement(container, element, oldNode, newNode, eventCb))
// }

// export function patchElement(
//   parent: Element,
//   element: Element | null,
//   oldNode: VNode | null,
//   newNode: VNode,
//   eventCb: EventCb,
//   isSvg?: boolean | undefined
// ): Element {
//   console.log(`${parent.nodeName}: `, parent.innerHTML)
//   // console.log({ parent, element, oldNode, newNode })
//   if (newNode === oldNode) {
//   } else if (isTextNode(oldNode) && isTextNode(newNode)) {
//     // patch Text Nodes
//     if (oldNode.name !== newNode.name) {
//       //for Vtext nodes node.name is the text value
//       ;(element as Element).nodeValue = newNode.name
//     }
//   } else if (
//     element == null ||
//     oldNode == null ||
//     oldNode.name !== newNode.name
//   ) {
//     // the first mount condition, element and oldNode is null,
//     // or if oldNode and newNode are different replace completely
//     var newElement = parent.insertBefore(
//       createElement((newNode = resolveNode(newNode)), eventCb, isSvg),
//       element
//     )

//     if (oldNode != null) removeElement(parent, oldNode)

//     element = newElement
//   } else {
//     // if nodes have same type, update properties/attributes
//     updateElement(
//       element,
//       oldNode.props,
//       newNode.props,
//       eventCb,
//       (isSvg = isSvg || newNode.name === 'svg')
//     )
//     // Handle children
//     let savedNode
//     let childNode

//     let oldKey
//     let oldChildren = oldNode.children
//     let oldChStart = 0
//     let oldChEnd = oldChildren.length - 1

//     let newKey
//     let newChildren = newNode.children
//     let newChStart = 0
//     let newChEnd = newChildren.length - 1
//     // if old and new keys are same
//     while (newChStart <= newChEnd && oldChStart <= oldChEnd) {
//       oldKey = getKey(oldChildren[oldChStart])
//       newKey = getKey(newChildren[newChStart])

//       if (oldKey == null || oldKey !== newKey) break

//       patchElement(
//         element,
//         oldChildren[oldChStart].element,
//         oldChildren[oldChStart],
//         (newChildren[newChStart] = resolveNode(
//           newChildren[newChStart],
//           oldChildren[oldChStart]
//         )),
//         eventCb,
//         isSvg
//       )

//       oldChStart++
//       newChStart++
//     }
//     // if old and new keys are same
//     while (newChStart <= newChEnd && oldChStart <= oldChEnd) {
//       oldKey = getKey(oldChildren[oldChEnd])
//       newKey = getKey(newChildren[newChEnd])

//       if (oldKey == null || oldKey !== newKey) break

//       patchElement(
//         element,
//         oldChildren[oldChEnd].element,
//         oldChildren[oldChEnd],
//         (newChildren[newChEnd] = resolveNode(
//           newChildren[newChEnd],
//           oldChildren[oldChEnd]
//         )),
//         eventCb,
//         isSvg
//       )

//       oldChEnd--
//       newChEnd--
//     }
//     // TBD
//     if (oldChStart > oldChEnd) {
//       while (newChStart <= newChEnd) {
//         element.insertBefore(
//           createElement(
//             (newChildren[newChStart] = resolveNode(newChildren[newChStart++])),
//             eventCb,
//             isSvg
//           ),
//           (childNode = oldChildren[oldChStart]) && childNode.element
//         )
//       }
//     } else if (newChStart > newChEnd) {
//       while (oldChStart <= oldChEnd) {
//         removeElement(element, oldChildren[oldChStart++])
//       }
//     } else {
//       var oldKeyed: { [index: string]: any } = createKeyMap(
//         oldChildren,
//         oldChStart,
//         oldChEnd
//       )
//       var newKeyed: { [index: string]: any } = {}

//       while (newChStart <= newChEnd) {
//         oldKey = getKey((childNode = oldChildren[oldChStart]))
//         newKey = getKey(
//           (newChildren[newChStart] = resolveNode(
//             newChildren[newChStart],
//             childNode
//           ))
//         )

//         if (
//           newKeyed[oldKey] ||
//           (newKey != null && newKey === getKey(oldChildren[oldChStart + 1]))
//         ) {
//           if (oldKey == null) {
//             removeElement(element, childNode)
//           }
//           oldChStart++
//           continue
//         }

//         if (newKey == null || oldNode.type === NODE_TYPE.RECYCLED) {
//           if (oldKey == null) {
//             // patch children when not keyed
//             patchElement(
//               element,
//               childNode && childNode.element,
//               childNode,
//               newChildren[newChStart],
//               eventCb,
//               isSvg
//             )
//             newChStart++
//           }
//           oldChStart++
//         } else {
//           if (oldKey === newKey) {
//             // patch element and children when keyed
//             patchElement(
//               element,
//               childNode.element,
//               childNode,
//               newChildren[newChStart],
//               eventCb,
//               isSvg
//             )
//             newKeyed[newKey] = true
//             oldChStart++
//             console.log('newKey :', newKey)
//           } else {
//             if ((savedNode = oldKeyed[newKey]) != null) {
//               patchElement(
//                 element,
//                 // insert key e before a
//                 element.insertBefore(
//                   savedNode.element,
//                   childNode && childNode.element
//                 ),
//                 savedNode,
//                 newChildren[newChStart],
//                 eventCb,
//                 isSvg
//               )
//               console.log('after e is befor a :', element.innerHTML)
//               newKeyed[newKey] = true
//             } else {
//               // keyed and oldNode[key] is null then create an new Dom Element
//               // basically when a new keyed item is present
//               patchElement(
//                 element,
//                 childNode && childNode.element,
//                 null,
//                 newChildren[newChStart],
//                 eventCb,
//                 isSvg
//               )
//             }
//           }
//           newChStart++
//         }
//       }

//       while (oldChStart <= oldChEnd) {
//         if (getKey((childNode = oldChildren[oldChStart++])) == null) {
//           removeElement(element, childNode)
//         }
//       }

//       for (var key in oldKeyed) {
//         if (newKeyed[key] == null) {
//           console.log('remove :', key)
//           removeElement(element, oldKeyed[key])
//         }
//       }
//     }
//   }
//   // @ts-ignore
//   // console.log('element :', element.outerHTML, parent.outerHTML)
//   return (newNode.element = element as Element)
// }

// function createKeyMap(children: VNode[], start: number, end: number) {
//   return children.reduce((keyMap: { [index: string]: VNode }, child: VNode) => {
//     if (child.key) keyMap[child.key] = child
//     return keyMap
//   }, {})
//   // for (var out = {}, key, node; start <= end; start++) {
//   //   if ((key = (node = children[start]).key) != null) {
//   //     out[key] = node
//   //   }
//   // }
//   // return out
// }
