// import { VNode, EventCb } from './global'
// import { resolveNode } from './h'
// import { createElement, removeElement, updateElement } from './element'
// import { isTextNode, getKey } from './utils'
// import { NODE_TYPE } from './constants'

// export const patch = function(
//   container: Element,
//   element: Element,
//   oldNode: VNode | null,
//   newNode: VNode,
//   eventCb: EventCb
// ) {
//   return patchElement(container, element, oldNode, newNode, eventCb)
// }

// export function patchElement(
//   parent: Element,
//   element: Element | null,
//   oldNode: VNode | null,
//   newNode: VNode,
//   eventCb: EventCb,
//   isSvg?: boolean | undefined
// ): Element {
//   !element && oldNode && (element = oldNode.element)
//   // console.log({ parent, element, oldNode, newNode })
//   if (newNode === oldNode) {
//   } else if (isTextNode(oldNode) && isTextNode(newNode)) {
//     // patch Text Nodes
//     if (oldNode.name !== newNode.name) {
//       //for Vtext nodes node.name is the text value
//       ;(element as Element).nodeValue = newNode.name
//     }
//   } else if (
//     element == null ||
//     oldNode == null ||
//     oldNode.name !== newNode.name
//   ) {
//     // the first mount condition, element and oldNode is null,
//     // or if oldNode and newNode are different replace completely
//     console.log('newNode.key :', newNode.key)
//     var newElement = parent.insertBefore(
//       createElement((newNode = resolveNode(newNode)), eventCb, isSvg),
//       element
//     )

//     if (oldNode != null) removeElement(parent, oldNode)

//     element = newElement
//   } else if (oldNode.element && oldNode.element !== element) {
//     // usually oldNode.element and element is same ,
//     // when its not then oldNode is the newNode because its keyed
//     console.log('oldNode replace :', oldNode.key)
//     // dont create new element if oldNode has element
//     element = parent.insertBefore(oldNode.element, element)

//     // if (oldNode != null) removeElement(parent, oldNode)
//   }

//   if (element && oldNode && newNode) {
//     // if nodes have same type, update properties/attributes
//     updateElement(
//       element,
//       oldNode.props,
//       newNode.props,
//       eventCb,
//       (isSvg = isSvg || newNode.name === 'svg')
//     )
//     // Handle children
//     let savedNode
//     let childNode

//     let oldChildren = oldNode.children
//     let oldChStart = 0
//     let oldChEnd = oldChildren.length - 1
//     oldNode.keys =
//       oldNode.keys || createKeyMap(oldChildren, oldChStart, oldChEnd)
//     // console.log('oldKeyed :', oldNode.keys)
//     let newChildren = newNode.children
//     let newChStart = 0
//     let newChEnd = newChildren.length - 1

//     // if (oldChStart > oldChEnd) {
//     //   while (newChStart <= newChEnd) {
//     //     element.insertBefore(
//     //       createElement(
//     //         (newChildren[newChStart] = resolveNode(newChildren[newChStart++])),
//     //         eventCb,
//     //         isSvg
//     //       ),
//     //       (childNode = oldChildren[oldChStart]) && childNode.element
//     //     )
//     //   }
//     // } else if (newChStart > newChEnd) {
//     //   while (oldChStart <= oldChEnd) {
//     //     removeElement(element, oldChildren[oldChStart++])
//     //   }
//     // } else {
//     // var oldKeyed: { [index: string]: any } = createKeyMap(
//     //   oldChildren,
//     //   oldChStart,
//     //   oldChEnd
//     // )

//     while (newChStart <= newChEnd) {
//       let oldKey = getKey((childNode = oldChildren[oldChStart]))
//       let newChildNode
//       let newKey = getKey(
//         (newChildNode = resolveNode(newChildren[newChStart], childNode))
//       )
//       let nextOldChildNode = oldChildren[oldChStart + 1]
//       // keyed items
//       if (newKey && oldNode.keys) {
//         if (!oldKey) {
//           console.log('newKey :', newKey)
//           throw new Error('non-keyed item in the keyed list')
//         }

//         // console.log('oldKeyed :', oldKeyed)
//         console.log({
//           newKey,
//           oldKey,
//           newChStart,
//           oldChStart,
//           nextKey: nextOldChildNode.key,
//           nextRemove: nextOldChildNode.remove
//         })
//         if (newKey === oldKey) {
//           patchElement(
//             element,
//             childNode.element,
//             childNode,
//             newChildNode,
//             eventCb,
//             isSvg
//           )
//           // oldNode.keys[oldKey] = true
//           oldChStart++
//         } else if (
//           newKey === getKey(nextOldChildNode) &&
//           nextOldChildNode.remove !== true
//         ) {
//           console.log('next old Key  :', newKey)
//           patchElement(
//             element,
//             nextOldChildNode.element,
//             nextOldChildNode,
//             newChildNode,
//             eventCb,
//             isSvg
//           )
//           // swap children
//           // oldChildren[oldChStart] = [
//           //   nextOldChildNode,
//           //   (oldChildren[oldChStart + 1] = childNode)
//           // ][0]
//           // @ts-ignore
//           oldChildren.splice(oldChStart, 1)
//           childNode.remove = true
//           oldChildren.push(childNode)
//           // savedNode.index = newChStart
//           // oldChildren.splice(newChStart, 0, savedNode)

//           oldChStart++
//         } else if ((savedNode = oldNode.keys[newKey]) != null) {
//           // savedNode.element = element.insertBefore(
//           //   savedNode.element,
//           //   childNode && childNode.element
//           // )
//           console.log('newKey but saved oldNode :', newKey)
//           if (nextOldChildNode.remove == true) {
//             patchElement(element, null, null, newChildNode, eventCb, isSvg)
//           } else {
//             patchElement(
//               element,
//               childNode.element,
//               // null,
//               savedNode,
//               // childNode,
//               newChildNode,
//               eventCb,
//               isSvg
//             )
//           }

//           // console.log('oldChildren :', oldChildren)
//           // oldChildren.splice(savedNode.index, 1)
//           savedNode.remove = true
//           oldChildren.splice(newChStart, 0, savedNode)

//           oldChStart++
//           console.log('oldChildren :', oldChildren)
//           // oldChildren[oldChStart] = [
//           //   oldChildren[oldChStart + 1],
//           //   (oldChildren[oldChStart + 1] = oldChildren[oldChStart])
//           // ][0]
//           // oldChStart++
//           // console.log(
//           //   `insert node: ${savedNode.name} before node: ${childNode.name}`
//           // )
//         } else {
//           // keyed and oldNode[key] is null then create an new Dom Element
//           // basically when a new keyed item is present
//           console.log('newKey but no oldKey:', newKey)
//           patchElement(
//             element,
//             childNode.element,
//             null,
//             newChildNode,
//             eventCb,
//             isSvg
//           )
//         }
//         if (!newNode.keys) newNode.keys = {}
//         newNode.keys[newKey] = newChildNode
//       } else {
//         // non-keyed items
//         patchElement(
//           element,
//           childNode.element,
//           childNode,
//           newChildNode,
//           eventCb,
//           isSvg
//         )
//         oldChStart++
//       }
//       newChStart++

//       // if (
//       //   newKeyed[oldKey] ||
//       //   (newKey != null && newKey === getKey(oldChildren[oldChStart + 1]))
//       // ) {
//       //   if (oldKey == null) {
//       //     removeElement(element, childNode)
//       //   }
//       //   oldChStart++
//       //   continue
//       // }
//     }

//     // while(oldKeyed[])

//     console.log({ oldChStart, oldChEnd })
//     while (oldChStart <= oldChEnd) {
//       if (getKey((childNode = oldChildren[oldChStart++])) != null) {
//         console.log({ childNode })
//         removeElement(element, childNode)
//       }
//     }
//     // for (var key in oldNode.keys) {
//     //   // if (newKeyed[key] == null) {
//     //   console.log('remove :', key)
//     //   removeElement(element, oldNode.keys[key])
//     //   // }
//     // }
//   }
//   // @ts-ignore
//   console.log(`${parent.nodeName}: `, parent.innerHTML)

//   // console.log('element :', element.outerHTML, parent.outerHTML)
//   return (newNode.element = element as Element)
// }

// function createKeyMap(children: VNode[], start: number, end: number) {
//   return children.reduce(
//     (keyMap: { [index: string]: VNode }, child: VNode, index: number) => {
//       if (child.key) {
//         keyMap[child.key] = child
//       }
//       return keyMap
//     },
//     {}
//   )
//   // for (var out = {}, key, node; start <= end; start++) {
//   //   if ((key = (node = children[start]).key) != null) {
//   //     out[key] = node
//   //   }
//   // }
//   // return out
// }
