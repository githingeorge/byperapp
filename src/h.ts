import {
  EMPTY_OBJECT,
  EMPTY_ARRAY,
  DEFAULT_NODE,
  TEXT_NODE,
  RECYCLED_NODE
} from './constants'
import { isLazyNode, getKey, hooks } from './utils'

type NODE_TYPE =
  | 0 // DEFAULT
  | 1 // RECYCLED
  | 2 // LAZY
  | 3 // TEXT

interface VProps {
  [index: string]: any | null
}
export interface VNode {
  name: string
  props: VProps
  children: VNode[]
  keys?: { [index: string]: any | null }
  element: Element | null
  key?: any
  type: NODE_TYPE
  patched?: boolean
}
export interface TextVNode extends VNode {
  type: 3
}
export interface LazyVNode extends VNode {
  type: 2
  lazy: any
  render: () => VNode
}

export const createVNode = (
  name: string,
  props: any,
  children: any[],
  element: Element | null = null,
  key = null,
  type: NODE_TYPE = DEFAULT_NODE
): VNode => {
  // console.log(name + ' => keys:', createKeyMap(children))
  return {
    name: name,
    props: props,
    children: children,
    element: element,
    key: key,
    type: type
  }
}

export const createTextVNode = (
  text: string,
  element: Element | null = null
) => {
  return createVNode(text, EMPTY_OBJECT, EMPTY_ARRAY, element, null, TEXT_NODE)
}

export const h = (name: string | Function, props?: VProps, ...rest: any[]) => {
  props = props || EMPTY_OBJECT
  rest = rest.flat()

  // const children = rest.map
  const children: any[] = rest.reduce((children, node) => {
    if (!(node === false || node === true || node == null)) {
      children.push(typeof node === 'object' ? node : createTextVNode(node))
    }
    return children
  }, [])
  // while (rest.length > 0) {
  //   let node = rest.pop()
  //   if (node === false || node === true || node == null) {
  //   } else {
  //     children.push(typeof node === 'object' ? node : createTextVNode(node))
  //   }
  // }

  if (typeof name === 'function') {
    let component = name
    for (const hook of hooks) {
      if (component.hasOwnProperty(hook)) {
        // @ts-ignore
        props[hook] = component[hook]
      }
    }
    return name(props, children)
  }

  return createVNode(name, props, children, null, props.key, DEFAULT_NODE)
}

export function shouldUpdate(a: {}, b: {}) {
  // @ts-ignore
  for (var k in a) if (a[k] !== b[k]) return true
  // @ts-ignore
  for (var k in b) if (a[k] !== b[k]) return true
}

export function resolveNode(newNode: VNode, oldNode?: VNode) {
  if (isLazyNode(newNode)) {
    return !oldNode ||
      shouldUpdate((newNode as LazyVNode).lazy, (oldNode as LazyVNode).lazy)
      ? (newNode as LazyVNode).render()
      : oldNode
  }
  return newNode
}

export function recycleElement(element: Element): VNode {
  return element.nodeType === TEXT_NODE
    ? createTextVNode(element.nodeValue as string, element)
    : createVNode(
        element.nodeName.toLowerCase(),
        EMPTY_OBJECT,
        EMPTY_ARRAY.map.call(element.childNodes, recycleElement),
        element,
        null,
        RECYCLED_NODE
      )
}

// const createKeyMap = (children: any[]) => {
//   return children.length > 0
//     ? children.reduce((keys, node) => {
//         if (keys && node && node.key) keys[node.key] = node
//         else return null
//         return keys
//       }, {})
//     : null
// }
