import { Dispatch } from '../app'

const effect = (props: any, dispatch: Dispatch) => {
  const eventListener = (event: Event) => dispatch(props.action, event)
  addEventListener('mousemove', eventListener)
  return () => removeEventListener('mousemove', eventListener)
}

export const moves = (props: any) => ({
  effect: effect,
  action: props.action
})
