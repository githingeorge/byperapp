import { Dispatch, EffectProps, Subscription } from '../app'

// import { Dispatch, Subscription, EffectProps } from '../byperapp'

const timeOutEffect = (props: EffectProps, dispatch: Dispatch) => {
  const id = setTimeout(() => {
    dispatch(props.action)
  }, props.interval)

  return () => clearTimeout(id)
}

function intervalEffect(props: EffectProps, dispatch: Dispatch) {
  const id = setInterval(() => {
    dispatch(props.action)
  }, props.interval)

  return () => {
    console.log('canceling id :', id)
    clearTimeout(id)
  }
}

const delay = (props: EffectProps) => [timeOutEffect, props]

// const tick = (props: any): Subscription => ({
//   effect: intervalEffect,
//   action: props.action,
//   interval: props.interval
// })

const tick = (props: EffectProps): Subscription => [intervalEffect, props]

export const Time = {
  delay,
  tick
}
