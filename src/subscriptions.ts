import { isArray } from 'util'
import { merge, UnTypedObj } from './utils'
import { Dispatch } from './app'

var isSameAction = function(a: any, b: any) {
  return isArray(a) && isArray(b) && a[0] === b[0] && typeof a[0] === 'function'
}

var shouldRestart = function(a: UnTypedObj, b: UnTypedObj) {
  for (var k in merge(a, b)) {
    if (a[k] === b[k] || isSameAction(a[k], b[k])) b[k] = a[k]
    else return true
  }
}

export const patchSub = (subs: any[], newSubs: any[], dispatch: Dispatch) => {
  // for (const { subscription, oldSubscription } in batch)
  //   if (subscription)
  //     if (oldSubscription) {
  //       if (changed(subscription, oldSubscription)) restart(subscription)
  //     } else start(subscription)
  //   else if (oldSubscription) cancel(oldSubscription)
  // console.log('subs :', subs)
  // console.log('newSubs :', newSubs)
  for (
    var i = 0, old, newSub, out = [];
    i < subs.length || i < newSubs.length;
    i++
  ) {
    old = subs[i]
    newSub = newSubs[i]
    // console.log('newSub[0] :', newSub[0])
    // console.log('newSub[1] :', newSub[1])
    out.push(
      newSub
        ? !old || newSub[0] !== old[0] || shouldRestart(newSub[1], old[1])
          ? [
              newSub[0],
              newSub[1],
              newSub[0](newSub[1], dispatch),
              old && old[2]()
            ]
          : old
        : // Cancel old Sub if newSub is empty
          old && old[2]()
    )
  }
  // console.log('out :', out)
  return out
}
