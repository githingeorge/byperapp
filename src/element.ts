import { isTextNode, merge, getKey, hooks } from './utils'
import { SVG_NS } from './constants'
import { resolveNode, VNode } from './h'
import { updateProperty } from './updateProperty'

export function createElement(node: VNode, lifecycle: any[], isSvg = false) {
  let element = isTextNode(node)
    ? document.createTextNode(node.name)
    : (isSvg = isSvg || node.name === 'svg')
    ? document.createElementNS(SVG_NS, node.name)
    : document.createElement(node.name)

  // node.keys = {}
  var props = node.props
  if (props) {
    if (props.oncreate) lifecycle.push(() => props.oncreate(element))

    for (var name in props) {
      updateProperty(
        element as (HTMLElement | SVGElement),
        name,
        null,
        props[name],
        // dispatch,
        isSvg
      )
    }
  }

  for (let i = 0; i < node.children.length; i++) {
    let childNode = resolveNode(node.children[i])

    element.appendChild(
      createElement((node.children[i] = childNode), lifecycle, isSvg)
    )
  }

  return (node.element = element as Element)
}
function removeChildren({ props, children, element }: VNode) {
  if (props && element) {
    for (var i = 0; i < children.length; i++) {
      removeChildren(children[i])
    }

    if (props.ondestroy) {
      props.ondestroy(element)
    }
  }
  return element as Element
}
export function removeElement(parent: Node, node: VNode) {
  // console.log('remove', {
  //   parent,
  //   node
  // })
  // debugger
  const { element, props } = node
  function done() {
    parent.removeChild(removeChildren(node))
  }
  var cb = props.onremove
  if (cb) {
    cb(element, done)
  } else {
    done()
  }
}

export function updateElement(
  el: Element,
  oldProps: any,
  newProps: any,

  lifecycle: any[],
  isRecycling = false,
  isSvg = false
) {
  for (var p in merge(oldProps, newProps)) {
    if (
      (p === 'value' || p === 'checked' || p === 'selected'
        ? p in el
        : oldProps[p]) !== newProps[p]
    ) {
      updateProperty(
        el as HTMLElement | SVGElement,
        p,
        oldProps[p],
        newProps[p],
        // dispatch,
        isSvg
      )
    }
  }
  let cb = isRecycling ? newProps.oncreate : newProps.onupdate

  if (cb) {
    lifecycle.push(() => cb(el, oldProps))
  }
}

// export const onCreate = (node: VNode, el: Element, dispatch: Dispatch) => {
//   let create: any
//   if ((create = node.props_events['create'])) dispatch(create, el)
// }
