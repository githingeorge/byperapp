export { h } from './h'
export { app } from './app'
export { Time } from './effects/time'

declare global {
  interface Element {
    _events: {
      [index: string]: EventListenerOrEventListenerObject
    }
    _lifecycle: CallableFunction[]
  }
  interface EventTarget {
    _events: {
      [index: string]: EventListenerOrEventListenerObject
    }
  }
  namespace JSX {
    interface IntrinsicElements {
      [index: string]: any
    }
  }
}
