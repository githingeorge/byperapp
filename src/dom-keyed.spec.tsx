import { h, app, Time } from './byperapp'
import { View } from './app'
const test_All = false
const testVdomToHtml = (name: string, trees: any, shouldTest = false) => {
  if (test_All || shouldTest) {
    test(name, (done: any) => {
      const state = { index: 0 }

      const actions = {
        next: (state: any) => {
          expect(document.body.innerHTML).toBe(
            trees[state.index].html.replace(/\s{2,}/g, '')
          )
          if (state.index === trees.length - 1) {
            // state = { index: 0 }
            // return { index: 0 }
            // console.log('done()')
            return done()
          }
          return [{ index: state.index + 1 }, effect]
        }
      }
      const effect = Time.delay({ action: actions.next, interval: 20 })

      const view: View = ({ state }) => {
        // console.log('state :', state)
        return trees[state.index].vdom
      }

      app({
        init: [state, effect],
        view: view,
        container: document.body
      })
      // console.log(document.body.innerHTML)
      // setTimeout(function() {
      //   expect(document.body.innerHTML).toBe(
      //     `<main>${tree.html.replace(/\s{2,}/g, '')}</main>`
      //   )
      // }, 0)
    })
  }
}

const setIdToKey = (key: any) => (element: Element) => {
  element.id = key
}

beforeEach(() => {
  document.body.innerHTML = ''
})

testVdomToHtml(
  'replace keyed',
  [
    {
      vdom: (
        <main>
          <div id="a" key="a">
            A
          </div>
        </main>
      ),
      html: `
        <main>
          <div id="a">A</div>
        </main>
      `
    },
    {
      vdom: (
        <main>
          <div id="b" key="b">
            B
          </div>
        </main>
      ),
      html: `
        <main>
          <div id="b">B</div>
        </main>
      `
    }
  ],
  true
)
testVdomToHtml(
  'remove keyed',
  [
    {
      vdom: (
        <main>
          <div id="a" key="a">
            A
          </div>
          <div id="b" key="b">
            B
          </div>
          <div id="c" key="c">
            C
          </div>
          <div id="d" key="d">
            D
          </div>
        </main>
      ),
      html: `
        <main>
          <div id="a">A</div>
          <div id="b">B</div>
          <div id="c">C</div>
          <div id="d">D</div>
        </main>
      `
    },
    {
      vdom: (
        <main>
          <div id="a" key="a">
            A
          </div>
          <div id="c" key="c">
            C
          </div>
          <div id="d" key="d">
            D
          </div>
        </main>
      ),
      html: `
        <main>
          <div id="a">A</div>
          <div id="c">C</div>
          <div id="d">D</div>
        </main>
      `
    }
  ],
  true
)

testVdomToHtml(
  'reorder keyed',
  [
    {
      vdom: (
        <main>
          <div id="a" key="a">
            A
          </div>
          <div id="b" key="b">
            B
          </div>
          <div id="c" key="c">
            C
          </div>
          <div id="d" key="d">
            D
          </div>
          <div id="e" key="e">
            E
          </div>
        </main>
      ),
      html: `
        <main>
          <div id="a">A</div>
          <div id="b">B</div>
          <div id="c">C</div>
          <div id="d">D</div>
          <div id="e">E</div>
        </main>
      `
    },
    {
      vdom: (
        <main>
          <div id="e" key="e">
            E1
          </div>
          <div id="a" key="a">
            A
          </div>
          <div id="b" key="b">
            B
          </div>
          <div id="c" key="c">
            C
          </div>
          <div id="d" key="d">
            D
          </div>
          <div id="f" key="f">
            F
          </div>
        </main>
      ),
      html: `
        <main>
          <div id="e">E1</div>
          <div id="a">A</div>
          <div id="b">B</div>
          <div id="c">C</div>
          <div id="d">D</div>
          <div id="f">F</div>
        </main>
      `
    },
    {
      vdom: (
        <main>
          <div id="e" key="e">
            E
          </div>
          <div id="d" key="d">
            D
          </div>
          <div id="a" key="a">
            A
          </div>
          <div id="c" key="c">
            C
          </div>
          <div id="b" key="b">
            B
          </div>
          <div id="f" key="f">
            F
          </div>
        </main>
      ),
      html: `
        <main>
          <div id="e">E</div>
          <div id="d">D</div>
          <div id="a">A</div>
          <div id="c">C</div>
          <div id="b">B</div>
          <div id="f">F</div>
        </main>
      `
    },
    {
      vdom: (
        <main>
          <div id="c" key="c">
            C
          </div>
          <div id="e" key="e">
            E
          </div>
          <div id="b" key="b">
            B
          </div>
          <div id="a" key="a">
            A
          </div>
          <div id="d" key="d">
            D
          </div>
        </main>
      ),
      html: `
        <main>
          <div id="c">C</div>
          <div id="e">E</div>
          <div id="b">B</div>
          <div id="a">A</div>
          <div id="d">D</div>
        </main>
      `
    }
  ],
  true
)

testVdomToHtml(
  'grow/shrink keyed',
  [
    {
      vdom: (
        <main>
          <div id="a" key="a">
            A
          </div>
          <div id="b" key="b">
            B
          </div>
          <div id="c" key="c">
            C
          </div>
          <div id="d" key="d">
            D
          </div>
          <div id="e" key="e">
            E
          </div>
        </main>
      ),
      html: `
        <main>
          <div id="a">A</div>
          <div id="b">B</div>
          <div id="c">C</div>
          <div id="d">D</div>
          <div id="e">E</div>
        </main>
      `
    },
    {
      vdom: (
        <main>
          <div id="a" key="a">
            A
          </div>
          <div id="c" key="c">
            C
          </div>
          <div id="d" key="d">
            D
          </div>
        </main>
      ),
      html: `
        <main>
          <div id="a">A</div>
          <div id="c">C</div>
          <div id="d">D</div>
        </main>
      `
    },
    {
      vdom: (
        <main>
          <div id="d" key="d">
            D
          </div>
        </main>
      ),
      html: `
        <main>
          <div id="d">D</div>
        </main>
      `
    },
    {
      vdom: (
        <main>
          <div id="a" key="a">
            A
          </div>
          <div id="b" key="b">
            B
          </div>
          <div id="c" key="c">
            C
          </div>
          <div id="d" key="d">
            D
          </div>
          <div id="e" key="e">
            E
          </div>
        </main>
      ),
      html: `
        <main>
          <div id="a">A</div>
          <div id="b">B</div>
          <div id="c">C</div>
          <div id="d">D</div>
          <div id="e">E</div>
        </main>
      `
    },
    {
      vdom: (
        <main>
          <div id="d" key="d">
            D
          </div>
          <div id="c" key="c">
            C
          </div>
          <div id="b" key="b">
            B
          </div>
          <div id="a" key="a">
            A
          </div>
        </main>
      ),
      html: `
        <main>
          <div id="d">D</div>
          <div id="c">C</div>
          <div id="b">B</div>
          <div id="a">A</div>
        </main>
      `
    }
  ],
  true
)
