import { h, app, Time } from './byperapp'
import { View } from './app'
const test_All = false
const testVdomToHtml = (name: string, trees: any, shouldTest = false) => {
  if (test_All || shouldTest) {
    test(name, (done: any) => {
      console.log(`testing ${name} =>`)
      const state = { index: 0 }

      const actions = {
        next: (state: any) => {
          expect(document.body.innerHTML).toBe(
            trees[state.index].html.replace(/\s{2,}/g, '')
          )
          if (state.index === trees.length - 1) {
            // state = { index: 0 }
            // return { index: 0 }
            // console.log('done()')
            return done()
          }
          return [{ index: state.index + 1 }, effect]
        }
      }
      const effect = Time.delay({ action: actions.next, interval: 20 })

      const view: View = ({ state }) => {
        // console.log('state :', state)
        return trees[state.index].vdom
      }

      app({
        init: [state, effect],
        view: view,
        container: document.body
      })
      // console.log(document.body.innerHTML)
      // setTimeout(function() {
      //   expect(document.body.innerHTML).toBe(
      //     `<main>${tree.html.replace(/\s{2,}/g, '')}</main>`
      //   )
      // }, 0)
    })
  }
}

const setIdToKey = (key: any) => (element: Element) => {
  element.id = key
}

beforeEach(() => {
  document.body.innerHTML = ''
})

testVdomToHtml(
  'replace element',
  [
    {
      vdom: <p />,
      html: `<p></p>`
    },
    {
      vdom: <div />,
      html: `<div></div>`
    }
  ],
  true
)
testVdomToHtml(
  'update element',
  [
    {
      vdom: <button />,
      html: `<button></button>`
    },
    {
      vdom: <button id="submit" />,
      html: `<button id="submit"></button>`
    }
  ],
  true
)
testVdomToHtml(
  'replace child',
  [
    {
      vdom: (
        <main>
          <div>foo</div>
        </main>
      ),
      html: `<main><div>foo</div></main>`
    },
    {
      vdom: (
        <main>
          <main>bar</main>
        </main>
      ),
      html: `<main><main>bar</main></main>`
    }
  ],
  true
)

testVdomToHtml(
  'remove text node',
  [
    {
      vdom: (
        <main>
          <div>foo</div>
          bar
        </main>
      ),
      html: `
        <main>
          <div>foo</div>
          bar
        </main>
      `
    },
    {
      vdom: (
        <main>
          <div>foo</div>
        </main>
      ),
      html: `
        <main>
          <div>foo</div>
        </main>
      `
    }
  ],
  true
)

testVdomToHtml(
  'styles',
  [
    {
      vdom: <div style="color: red;" />,
      html: `<div style="color: red;"></div>`
    },
    {
      vdom: <div style={{ color: 'red', fontSize: '1em', '--foo': 'red' }} />,
      html: `<div style="color: red; font-size: 1em;"></div>`
    },

    {
      vdom: <div style={{ color: 'blue', display: 'flex', '--foo': 'blue' }} />,
      html: `<div style="color: blue; display: flex;"></div>`
    },
    {
      vdom: <div style="background-color: blue;" />,
      html: `<div style="background-color: blue;"></div>`
    },
    {
      vdom: <div style={null} />,
      html: `<div style=""></div>`
    },
    {
      vdom: <div style="" />,
      html: `<div style=""></div>`
    }
  ],
  true
)

testVdomToHtml(
  'update element data',
  [
    {
      vdom: <div id="foo" class="bar" />,
      html: `<div id="foo" class="bar"></div>`
    },
    {
      vdom: <div id="foo" class="baz" />,
      html: `<div id="foo" class="baz"></div>`
    }
  ],
  true
)

testVdomToHtml(
  'removeAttribute',
  [
    {
      vdom: <div id="foo" class="bar" />,
      html: `<div id="foo" class="bar"></div>`
    },
    {
      vdom: <div />,
      html: `<div></div>`
    }
  ],
  true
)

testVdomToHtml(
  'skip setAttribute for functions',
  [
    {
      vdom: <div oncreate={() => {}} />,
      html: `<div></div>`
    }
  ],
  true
)

testVdomToHtml(
  'setAttribute true',
  [
    {
      vdom: <div enabled="true" />,
      html: `<div enabled="true"></div>`
    }
  ],
  true
)

testVdomToHtml(
  'a list with empty text nodes',
  [
    {
      vdom: (
        <ul>
          <li />
          <div>foo</div>
        </ul>
      ),
      html: `<ul><li></li><div>foo</div></ul>`
    },
    {
      vdom: (
        <ul>
          <li />
          <li />
          <div>foo</div>
        </ul>
      ),
      html: `<ul><li></li><li></li><div>foo</div></ul>`
    },
    {
      vdom: (
        <ul>
          <li />
          <div>foo</div>
        </ul>
      ),
      html: `<ul><li></li><div>foo</div></ul>`
    }
    // {
    //   vdom: (
    //     <ul>
    //       <li />
    //       <li />
    //       <li />
    //     </ul>
    //   ),
    //   html: `<ul><li></li><li></li><li></li></ul>`
    // },
    // {
    //   vdom: <ul />,
    //   html: `<ul></ul>`
    // }
  ],
  true
)

testVdomToHtml(
  'grow/shrink list',
  [
    {
      vdom: (
        <main>
          <div id="a">A</div>
          <div id="b">B</div>
          <div id="c">C</div>
          <div id="d">D</div>
          <div id="e">E</div>
        </main>
      ),
      html: `
        <main>
          <div id="a">A</div>
          <div id="b">B</div>
          <div id="c">C</div>
          <div id="d">D</div>
          <div id="e">E</div>
        </main>
      `
    },
    {
      vdom: (
        <main>
          <div id="a">A</div>
          <div id="c">C</div>
          <div id="d">D</div>
        </main>
      ),
      html: `
        <main>
          <div id="a">A</div>
          <div id="c">C</div>
          <div id="d">D</div>
        </main>
      `
    },
    {
      vdom: (
        <main>
          <div id="d">D</div>
        </main>
      ),
      html: `
        <main>
          <div id="d">D</div>
        </main>
      `
    },
    {
      vdom: (
        <main>
          <div id="a">A</div>
          <div id="b">B</div>
          <div id="c">C</div>
          <div id="d">D</div>
          <div id="e">E</div>
        </main>
      ),
      html: `
        <main>
          <div id="a">A</div>
          <div id="b">B</div>
          <div id="c">C</div>
          <div id="d">D</div>
          <div id="e">E</div>
        </main>
      `
    },
    {
      vdom: (
        <main>
          <div id="d">D</div>
          <div id="c">C</div>
          <div id="b">B</div>
          <div id="a">A</div>
        </main>
      ),
      html: `
        <main>
          <div id="d">D</div>
          <div id="c">C</div>
          <div id="b">B</div>
          <div id="a">A</div>
        </main>
      `
    }
  ]
  // true
)

// testVdomToHtml('elements with falsey values', [
//   {
//     vdom: <div data-test={'0'} />,
//     html: `<div data-test="0"></div>`
//   },
//   {
//     vdom: <div data-test={0} />,
//     html: `<div data-test="0"></div>`
//   },
//   {
//     vdom: <div data-test={null} />,
//     html: `<div></div>`
//   },
//   {
//     vdom: <div data-test={false} />,
//     html: `<div></div>`
//   },
//   {
//     vdom: <div data-test={undefined} />,
//     html: `<div></div>`
//   }
// ])

// testVdomToHtml('update element with dynamic props', [
//   {
//     vdom: (
//       <input
//         type="text"
//         value="foo"
//         onupdate={element => {
//           expect(element.value).toBe('foo')
//         }}
//       />
//     ),
//     html: `<input type="text">`
//   },
//   {
//     vdom: (
//       <input
//         type="text"
//         value="bar"
//         onupdate={element => {
//           expect(element.value).toBe('bar')
//         }}
//       />
//     ),
//     html: `<input type="text">`
//   }
// ])

// testVdomToHtml("don't touch textnodes if equal", [
//   {
//     vdom: (
//       <main
//         oncreate={e => {
//           e.childNodes[0].textContent = 'foobar'
//         }}
//       >
//         foobar
//       </main>
//     ),
//     html: `<main>foobar</main>`
//   },
//   {
//     vdom: <main>foobar</main>,
//     html: `<main>foobar</main>`
//   }
// ])

// testVdomToHtml('input list attribute', [
//   {
//     vdom: <input list="foobar" />,
//     html: `<input list="foobar">`
//   }
// ])

// testVdomToHtml('events', [
//   {
//     vdom: (
//       <button
//         oncreate={element => element.dispatchEvent(new Event('click'))}
//         onclick={event => {
//           event.currentTarget.id = 'clicked'
//         }}
//       />
//     ),
//     html: `<button id="clicked"></button>`
//   }
// ])

// testVdomToHtml('boolean attributes', [
//   {
//     vdom: (
//       <main>
//         <input
//           checked={true}
//           spellcheck="true"
//           autocomplete="on"
//           translate="yes"
//         />
//         <input
//           checked={false}
//           spellcheck="false"
//           autocomplete="off"
//           translate="no"
//         />
//       </main>
//     ),
//     html: `
//         <main>
//           <input spellcheck="true" autocomplete="on" translate="yes">
//           <input spellcheck="false" autocomplete="off" translate="no">
//         </main>
//       `
//   }
// ])
