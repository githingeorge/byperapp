import { h, app, Time } from './byperapp'
import { View } from './app'

beforeEach(() => {
  document.body.innerHTML = ''
})

test('oncreate', (done: any) => {
  console.log(`testing ${name} =>`)
  const state = { index: 'foo' }

  // const effect = Time.delay({ action: actions.next, interval: 20 })

  const view = (state: any) => (
    <div
      oncreate={(element: Element) => {
        console.log('oncreate')
        element.className = 'bar'
        expect(document.body.innerHTML).toBe(`<div class="bar">foo</div>`)
        done()
      }}
    >
      foo
    </div>
  )

  app({
    init: state,
    view: view,
    container: document.body
  })
})

test('oncreate', (done: any) => {
  console.log(`testing ${name} =>`)
  const state = { index: 'foo' }

  // const effect = Time.delay({ action: actions.next, interval: 20 })

  const view = (state: any) => (
    <div
      oncreate={(element: Element) => {
        console.log('oncreate')
        element.className = 'foo'
        expect(document.body.innerHTML).toBe(
          `<div class="foo">foo<div class="bar">bar</div></div>`
        )
        done()
      }}
    >
      foo
      <div
        oncreate={(element: Element) => {
          console.log('oncreate')
          element.className = 'bar'
          expect(document.body.innerHTML).toBe(
            `<div>foo<div class="bar">bar</div></div>`
          )
        }}
      >
        bar
      </div>
    </div>
  )

  app({
    init: state,
    view: view,
    container: document.body
  })
})

test('onupdate', (done: any) => {
  const state = { value: 'foo' }
  const actions = {
    setValue: (state: any) => {
      console.log('setValue:', 'bar')
      return { value: 'bar' }
    },
    onUpdate: (element: Element, oldProps: any) => {
      console.log('onupdate')

      expect(element.classList.value).toBe('bar')
      expect(oldProps.class).toBe('foo')
      done()
    }
  }

  const view: View = ({ state }) => {
    console.log('state :', state)
    return (
      <div
        class={state.value}
        oncreate={() => [actions.setValue]}
        onupdate={actions.onUpdate}
      >
        {state.value}
      </div>
    )
  }

  app({ init: state, view, container: document.body })
})

test('onclick event', (done: any) => {
  const state = { value: 'foo' }

  const setId = (state: any, event: Event) => {
    // @ts-ignore
    event.currentTarget.id = 'clicked'
    // @ts-ignore
    expect(event.currentTarget.id).toBe('clicked')
    done()
  }
  const view: View = ({ state, eventHandler }) => {
    console.log('state :', state)
    return (
      <button
        oncreate={(el: Element) => {
          console.log('clicked :')
          el.dispatchEvent(new Event('click'))
          debugger
        }}
        onclick={eventHandler(setId)}
      />
    )
  }

  app({ init: state, view, container: document.body })
})

test('onremove', (done: any) => {
  const state = { value: 'foo', remove: false }

  const setRemove = (state: any, event: Event) => {
    return { ...state, remove: true }
  }
  const view: View = ({ state, eventHandler }) => {
    console.log('state :', state)
    return (
      <div>
        <button
          oncreate={(el: Element) => {
            el.dispatchEvent(new Event('click'))
          }}
          onclick={eventHandler(setRemove)}
        />
        {!state.remove ? (
          <p
            onremove={(el: Element, remove: any) => {
              remove()
              expect(el.parentElement).toBeNull()
              done()
            }}
          />
        ) : null}
      </div>
    )
  }

  app({ init: state, view, container: document.body })
})

test('onremove/ondestroy', done => {
  let detached = false

  const state = {
    value: true
  }

  const actions = {
    toggle: (state: any) => ({ value: !state.value })
  }

  const view: View = ({ state }) =>
    state.value ? (
      <ul oncreate={() => [actions.toggle]}>
        <li />
        <li
          ondestroy={() => {
            detached = true
          }}
          onremove={(element: Element, remove: any) => {
            expect(detached).toBe(false)
            remove()
            expect(detached).toBe(true)
            done()
          }}
        />
      </ul>
    ) : (
      <ul>
        <li />
      </ul>
    )

  app({ init: state, view, container: document.body })
})
