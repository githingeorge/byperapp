import { resolveNode, VNode } from './h'
import { createElement, removeElement, updateElement } from './element'
import { isTextNode, getKey } from './utils'
import { Dispatch } from './app'

export const patch = function(
  container: Element,
  element: Element,
  oldNode: VNode | null,
  newNode: VNode,
  dispatch: Dispatch,
  isRecycling: boolean
) {
  let lifecycle: any[] = []

  let nodes = patchElement(
    container,
    oldNode,
    newNode,
    null,
    // oldNode && oldNode.element,
    false,
    dispatch,
    lifecycle,
    isRecycling
  )
  while (lifecycle.length) dispatch(lifecycle.pop()())

  return nodes
}

export function patchElement(
  parent: Element,
  // element: Element | null,
  oldNode: VNode | null,
  newNode: VNode,
  positionEl: Element | null,
  after = false,
  dispatch: Dispatch,
  lifecycle: any[],
  isRecycling = false,
  isSvg?: boolean | undefined
): Element {
  let element = oldNode && oldNode.element

  // console.log({ oldNode, newNode, positionEl })
  if (newNode === oldNode) {
  } else if (isTextNode(oldNode) && isTextNode(newNode)) {
    // patch Text Nodes
    if (oldNode.name !== newNode.name) {
      //for Vtext nodes node.name is the text value
      ;(element as Element).nodeValue = newNode.name
    }
    return (newNode.element = element as Element)
  } else if (oldNode == null || oldNode.name !== newNode.name) {
    // the first mount condition, element and oldNode is null,
    // or if oldNode and newNode are different replace completely
    var newElement = parent.insertBefore(
      createElement((newNode = resolveNode(newNode)), lifecycle, isSvg),
      positionEl
    )

    // if (newNode.props && newNode.props.oncreate)
    //   onCreate(newNode, newElement, dispatch)

    if (oldNode != null && !positionEl) removeElement(parent, oldNode)

    // element = newElement
    // console.log(`${parent.nodeName}: `, parent.innerHTML)

    return (newNode.element = newElement as Element)
  } else if (oldNode.element && positionEl) {
    // if (oldNode.element === positionEl)
    // console.log(`Insert ${after ? 'before' : 'after'}: ${oldNode.name}`)
    positionEl.insertAdjacentElement(
      after ? 'afterend' : 'beforebegin',
      oldNode.element
    )

    // if (oldNode != null) removeElement(parent, oldNode)
  }

  if (element && oldNode && newNode) {
    // console.log('update :', newNode.name, newNode.type)
    // if nodes have same type, update properties/attributes
    updateElement(
      element,
      oldNode.props,
      newNode.props,
      lifecycle,
      isRecycling,
      (isSvg = isSvg || newNode.name === 'svg')
    )
    // Handle children
    let savedNode
    let childNode

    let oldChildren = oldNode.children
    let oldChStart = 0
    let oldChEnd = oldChildren.length - 1
    oldNode.keys =
      oldNode.keys || createKeyMap(oldChildren, oldChStart, oldChEnd)
    // console.log('oldKeyed :', oldNode.keys)
    let newChildren = newNode.children
    let newChStart = 0
    let newChEnd = newChildren.length - 1

    while (newChStart <= newChEnd) {
      let oldKey = getKey((childNode = oldChildren[oldChStart]))

      let newChildNode
      let newKey = getKey(
        (newChildNode = resolveNode(newChildren[newChStart], childNode))
      )
      let nextOldChildNode = oldChildren[oldChStart + 1]
      // keyed items
      if (newKey && oldNode.keys) {
        if (!oldKey) {
          console.log('newKey :', newKey)
          throw new Error('non-keyed item in the keyed list')
        }

        // console.log('oldKeyed :', oldKeyed)
        // console.log({
        //   newKey,
        //   oldKey,
        //   newChStart,
        //   oldChStart,
        //   nextKey: nextOldChildNode && nextOldChildNode.key,
        //   nextPatched: nextOldChildNode && nextOldChildNode.patched
        // })
        if (newKey === oldKey) {
          patchElement(
            element,
            childNode,
            newChildNode,
            null,
            false,
            dispatch,
            lifecycle,
            isSvg
          )
          childNode.patched = true
          oldChStart++
        } else if (
          newKey === getKey(nextOldChildNode) &&
          nextOldChildNode.patched !== true
        ) {
          // console.log('next old Key  :', newKey)
          patchElement(
            element,
            nextOldChildNode,
            newChildNode,
            null,
            false,
            dispatch,
            lifecycle,
            isSvg
          )
          nextOldChildNode.patched = true
          oldChildren.push(childNode)
          oldChStart++
        } else if (
          (savedNode = oldNode.keys[newKey]) != null &&
          !savedNode.patched
        ) {
          // console.log('newKey but saved oldNode :', newKey)
          patchElement(
            element,
            savedNode,
            newChildNode,
            childNode.element,
            childNode.patched,
            dispatch,
            lifecycle,
            isSvg
          )
          savedNode.patched = true
          if (childNode.patched) {
            oldChildren[newChStart - 1] = [
              childNode,
              (oldChildren[newChStart] = savedNode)
            ][0]
          } //oldChildren.splice(newChStart, 1)
          else oldChildren.splice(newChStart, 0, savedNode)

          oldChStart++
          // console.log('oldChildren :', oldChildren)
        } else {
          // keyed and oldNode[key] is null then create an new Dom Element
          // basically when a new keyed item is present
          // console.log('newKey but no oldKey:', newKey)
          patchElement(
            element,
            null,
            newChildNode,
            childNode.patched ? null : childNode.element, // childNode.element,
            !childNode.patched,
            dispatch,
            lifecycle,
            isSvg
          )
          oldChildren.push(childNode)
        }
        if (!newNode.keys) newNode.keys = {}
        newNode.keys[newKey] = newChildNode
      } else {
        // non-keyed items
        if (childNode) {
          const differentNode = childNode.name !== newChildNode.name
          patchElement(
            element,
            childNode,
            // differentNode ? null : childNode,
            newChildNode,
            differentNode ? childNode.element : null,
            // childNode.element,
            false,
            dispatch,
            lifecycle,
            isSvg
          )
          // oldChEnd++

          if (differentNode) {
            oldChildren.splice(newChStart, 0, childNode)
            if (isTextNode(newChildNode)) oldChStart++
          } else {
            childNode.patched = true
            oldChStart++
          }
        } else {
          patchElement(
            element,
            null,
            newChildNode,
            null,
            false,
            dispatch,
            lifecycle,
            isSvg
          )
        }
      }
      newChStart++

      //
    }

    // while(oldKeyed[])

    // console.log({ oldChStart, oldChEnd })
    while (oldChStart <= oldChEnd) {
      if ((childNode = oldChildren[newChStart]) != null && !childNode.patched) {
        // console.log('remove: ', childNode.name, ' from children')
        removeElement(element, childNode)
        oldChStart++
      }
      newChStart++
      // if (newChStart > 1000) break
    }
    // for (var key in oldNode.keys) {
    //   // if (newKeyed[key] == null) {
    //   console.log('remove :', key)
    //   removeElement(element, oldNode.keys[key])
    //   // }
    // }
  }
  // @ts-ignore
  // console.log(`${parent.nodeName}: `, parent.innerHTML)

  // console.log('element :', element.outerHTML, parent.outerHTML)
  return (newNode.element = element as Element)
}

function createKeyMap(children: VNode[], start: number, end: number) {
  return children.reduce(
    (keyMap: { [index: string]: VNode }, child: VNode, index: number) => {
      if (child.key) {
        keyMap[child.key] = child
      }
      return keyMap
    },
    {}
  )
  // for (var out = {}, key, node; start <= end; start++) {
  //   if ((key = (node = children[start]).key) != null) {
  //     out[key] = node
  //   }
  // }
  // return out
}
