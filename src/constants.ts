// export enum NODE_TYPE {
//   DEFAULT = 0,
//   RECYCLED = 1,
//   LAZY = 2,
//   TEXT = 3
// }

export const DEFAULT_NODE = 0
export const RECYCLED_NODE = 1
export const LAZY_NODE = 2
export const TEXT_NODE = 3

export const XLINK_NS = 'http://www.w3.org/1999/xlink'
export const SVG_NS = 'http://www.w3.org/2000/svg'

export const EMPTY_OBJECT = {}
export const EMPTY_ARRAY: any[] = []

export const map = EMPTY_ARRAY.map
export const isArray = Array.isArray
