import { h } from './h'
import { NODE_TYPE } from './constants'

test('<div></div> => vNode', () => {
  // let div = h('div')
  let div = <div />
  expect({
    name: 'div',
    props: {},
    children: [],
    element: null,
    key: null,
    type: NODE_TYPE.DEFAULT
  }).toEqual(div)
})
test('<div>Hello</div> => vNode', () => {
  // let div = h('div')
  let div = <div>Hello</div>
  expect({
    name: 'div',
    props: {},
    children: [
      {
        name: 'Hello',
        props: {},
        children: [],
        element: null,
        key: null,
        type: NODE_TYPE.TEXT
      }
    ],
    element: null,
    key: null,
    type: NODE_TYPE.DEFAULT
  }).toEqual(div)
})

test('<div><h1>Hello</h1></div> => vNode', () => {
  // let div = h('div')
  let div = (
    <div>
      <h1>Hello</h1>
    </div>
  )
  expect({
    name: 'div',
    props: {},
    children: [
      {
        name: 'h1',
        props: {},
        children: [
          {
            name: 'Hello',
            props: {},
            children: [],
            element: null,
            key: null,

            type: NODE_TYPE.TEXT
          }
        ],
        element: null,
        key: null,

        type: NODE_TYPE.DEFAULT
      }
    ],
    element: null,
    key: null,
    type: NODE_TYPE.DEFAULT
  }).toEqual(div)
})
