import { XLINK_NS, isArray } from './constants'
import { merge, hooks } from './utils'

function createClass(obj: any) {
  var tmp = typeof obj
  var out = ''

  if (tmp === 'string' || tmp === 'number') return obj || ''

  if (isArray(obj) && obj.length > 0) {
    for (var i = 0, length = obj.length; i < length; i++) {
      if ((tmp = createClass(obj[i])) !== '') out += (out && ' ') + tmp
    }
  } else {
    for (let i in obj) {
      if (obj[i]) out += (out && ' ') + i
    }
  }

  return out
}

export function updateProperty(
  element: HTMLElement | SVGElement,
  name: string,
  oldValue: any,
  newValue: any,
  // dispatch: Dispatch,
  isSvg: boolean
) {
  if (name === 'key') {
  } else if (name === 'style') {
    if (typeof newValue == 'string') {
      element.setAttribute('style', newValue)
    } else {
      if (typeof oldValue === 'string') {
        oldValue = {}
        let style = element.style
        for (var i = 0; i < style.length; i++) {
          var name = style[i]
          var value = style.getPropertyValue(name)
          // convert kebab-case style props to camelCase
          name = name.replace(/-([a-z])/g, function(g) {
            return g[1].toUpperCase()
          })
          oldValue[name] = value
        }
      }
      let style = Object.assign({}, oldValue, newValue)
      Object.keys(style).forEach(key => {
        if (key[0] === '-') {
          element.style.setProperty(key, style)
        } else {
          // @ts-ignore
          element.style[key] =
            newValue == null ? '' : newValue[key] == null ? '' : newValue[key]
        }
        // element.style.setProperty(key, newValue[key])
      })
      // console.log('element.outerHTML :', element.outerHTML)
      // for (var i in merge(oldValue, newValue)) {
      //   var style = newValue == null || newValue[i] == null ? '' : newValue[i]
      //   if (i[0] === '-') {
      //     element.style.setProperty(i, style)
      //   } else {
      //     // @ts-ignore
      //     element.style[i] = style
      //   }
      // }
    }
  } else if (name === 'class') {
    if ((newValue = createClass(newValue))) {
      element.setAttribute(name, newValue)
    } else {
      element.removeAttribute(name)
    }
  } else {
    if (name[0] === 'o' && name[1] === 'n') {
      // if (!element._events) element._events = {}

      // element._events[
      //   (name = name.slice(2).toLowerCase())
      // ] = newValue as EventCb
      name = name.slice(2).toLowerCase()
      if (newValue == null) {
        element.removeEventListener(name, oldValue)
      } else if (oldValue == null && hooks.indexOf(name) == -1) {
        element.addEventListener(name, newValue)
      }
    } else {
      var nullOrFalse = newValue == null || newValue === false
      if (
        name in element &&
        name !== 'list' &&
        name !== 'draggable' &&
        name !== 'spellcheck' &&
        name !== 'translate' &&
        !isSvg
      ) {
        //@ts-ignore
        element[name] = newValue == null ? '' : newValue
        if (nullOrFalse) {
          element.removeAttribute(name)
        }
      } else {
        var ns = isSvg && name !== (name = name.replace(/^xlink:?/, ''))
        if (ns) {
          if (nullOrFalse) {
            element.removeAttributeNS(XLINK_NS, name)
          } else {
            element.setAttributeNS(XLINK_NS, name, newValue)
          }
        } else {
          if (nullOrFalse) {
            element.removeAttribute(name)
          } else {
            element.setAttribute(name, newValue)
          }
        }
      }
    }
  }
}
