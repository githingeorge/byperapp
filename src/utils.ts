import { TEXT_NODE, LAZY_NODE } from './constants'
import { VNode, TextVNode, LazyVNode } from './h'

export interface UnTypedObj {
  [index: string]: any
}

export function isTextNode(node: VNode | null): node is TextVNode {
  return node != null && node.type == TEXT_NODE
}

export function isLazyNode(node: VNode | null): node is LazyVNode {
  return node != null && node.type == LAZY_NODE
}
// export function updateTextNode(oldNode: VNode, newNode: VNode) {
//   if (oldNode.name !== newNode.name) {
//     element.nodeValue = newNode.name
//   }
// }

export function merge(a: UnTypedObj, b: UnTypedObj) {
  var target: UnTypedObj = {}

  for (var i in a) target[i] = a[i]
  for (var i in b) target[i] = b[i]

  return target
}

export const getKey = (node: VNode) => (node == null ? null : node.key)

export const hooks = ['create', 'update', 'remove', 'destroy']
