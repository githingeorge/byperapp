import { patch } from './patch.2'
import { isArray, EMPTY_ARRAY } from './constants'
import { patchSub } from './subscriptions'
import { recycleElement, VNode } from './h'

type EventCb = EventListenerOrEventListenerObject
interface EventHandler {
  (action: any): EventListenerOrEventListenerObject
}
export interface Dispatch {
  (o: any, ...props: any): void
}

interface AppProps {
  container: Element
  subscriptions?: (state: any) => [Subscription | boolean]
  view: any
  init: any
}
export interface EffectProps {
  action: CallableFunction
  interval?: number
}
export interface Effect {
  (props: EffectProps, dispatch: Dispatch): any
}
export interface Action extends CallableFunction {}

export interface Subscription extends Array<Effect | EffectProps> {
  0: Effect
  1: EffectProps
}

export interface View {
  ({ state, eventHandler }: { state: any; eventHandler: EventHandler }):
    | VNode
    | VNode[]
}

export function app(props: AppProps) {
  let container = props.container
  let element = container && container.children[0]
  let oldNode = element && recycleElement(element)
  let isRecycling = true
  let subs = props.subscriptions
  let view = props.view
  let lock = false
  let state: {}
  let sub: any[] | never[] = []
  // console.log('props :', props)
  let setState = function(newState: {}) {
    if ('value' in newState) console.log('newState :', newState)
    if (!(state === newState || lock)) {
      lock = true
      defer(render)
    }
    state = newState
  }
  const eh: EventHandler = (action: any) => (event: Event) => {
    // @ts-ignore
    dispatch(action, event)
    // event.currentTarget && event.currentTarget._events[event.type],
  }
  const dispatch: Dispatch = (action: {}, ...props: any) => {
    if (typeof action === 'function') {
      dispatch(action(state, ...props)) // obj = action
    } else if (isArray(action)) {
      if (typeof action[0] === 'function') {
        dispatch(action[0](state, action[1], ...props))
      } else {
        flatten(action.slice(1)).map(function(fx) {
          // fx[0] = effect
          fx && fx[0](fx[1], dispatch)
        }, setState(action[0]))
      }
    } else if (action !== undefined) {
      setState(action)
    }
  }

  function render() {
    // console.log('defer :', defer)
    lock = false
    if (subs) sub = patchSub(sub, flatten(subs(state)), dispatch)
    // console.log('oldNode :', oldNode)
    if (view) {
      element = patch(
        container,
        element,
        oldNode,
        (oldNode = view({ state, eventHandler: eh })),
        dispatch,
        isRecycling
      )
    }

    isRecycling = false
  }

  dispatch(props.init)
}
let defer = requestAnimationFrame || setTimeout
// typeof Promise === 'function'
//   ? function(cb: any) {
//       Promise.resolve().then(cb)
//     }

export function flatten(arr: any[]): any[] {
  // console.log('arr :', arr, arr[0])
  return arr.reduce(function(out, obj) {
    return out.concat(
      !obj || obj === true
        ? false
        : typeof obj[0] === 'function'
        ? [obj]
        : flatten(obj)
    )
  }, EMPTY_ARRAY)
}

// export { h } from './h'
